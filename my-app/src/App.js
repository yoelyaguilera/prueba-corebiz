import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Menu from './util/navBar.js';
import Slider from './util/slider.js';
import React from 'react';
import Products from './util/products.js';

function App() {
  return (
    <div className="App">
		<Menu/>
		<Slider/>
		

		<Products/>

    </div>
  );
}

export default App;
