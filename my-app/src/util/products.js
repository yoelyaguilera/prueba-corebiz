import { Component } from 'react';
import { Card, Button } from 'react-bootstrap';
import React, {useState, useEffect} from 'react';

function Products1() {
	const url = 'https://5d8cdb5a443e3400143b4bea.mockapi.io/corebizchile/products'
	const [products1,setProducts1] = useState()
	const fetchApi = async () =>{
		const response = await fetch(url) 
		const responseJSON = await response.json()
		setProducts1 (responseJSON)
	}
	useEffect(() => {
		fetchApi()
	},[])

  return (
  	<ul>
  	<br/>
  	<h2> Mas Vendidos</h2>
  	<br/>
  	<div className="row">
  	
{!products1 ? 'cargando' : products1.map((product1,index)=>{
	return (
		
		<Card className="mr-sm-4" style={{ width: '18rem' }}>
		  <Card.Img variant="top" src={product1.img} />
		  <Card.Body>
		    <Card.Title>{product1.product} </Card.Title>
		    <Card.Text>{product1.description}</Card.Text>
		    <Card.Text style={{fontWeight: 'bold'}}>{product1.price}</Card.Text>
		    <Button variant="primary">Comprar</Button>
		  </Card.Body>
		</Card>
		

		)
		
})}
</div>
</ul>
    	
  );
}

export default Products1;

