import React from 'react';
import { Component } from 'react';
import { Navbar, Nav, NavDropdown, Form, FormControl, Button} from 'react-bootstrap';
import Logo from './../images/logo.png';
import search from './../images/search.png';
import account from './../images/account.png';
import shopping from './../images/shopping-cart.png';


export default class Menu extends Component {
  constructor(props) {super(props);}
  componentWillMount() {}
  componentWillReceiveProps(nextProps) {}

  render() {
    return (

    <div className="row" style={{backgroundColor: '#fafafa'}}>
      	<Navbar bg="light" expand="lg" style={{ marginLeft: 'auto', marginRight: 'auto'}}>
		  	<Navbar.Brand href="#home">
		  		<img src={Logo} height="30" className="d-inline-block align-top"/>{' '}
		  	</Navbar.Brand>    
		  	<Form inline>
		      <FormControl type="text" placeholder="Que estas buscando?" className="mr-sm-2 search" style={{width: '500px'}}/>
		      <Button className="btnsearch" >
		      	<img src={search} height="30" className="d-inline-block align-top"/>{' '}</Button>
		    </Form>
		    <Nav className="mr-auto">
		      <Nav.Link href="#home">
		      	<img src={account} height="20" className="d-inline-block align-top"/>Yoely Aguilera</Nav.Link>
		      <Nav.Link href="#link">
		      	<img src={shopping} height="20" className="d-inline-block align-top"/></Nav.Link>
		    </Nav>
		</Navbar>
	</div>

    );
  }
}