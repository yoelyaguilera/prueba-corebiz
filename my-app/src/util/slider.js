import React from 'react';
import { Component } from 'react';
import { Carousel } from 'react-bootstrap';
import Banner from './../images/Banner.jpg';

export default class Slider extends Component {
  constructor(props) {super(props);}
  componentWillMount() {}
  componentWillReceiveProps(nextProps) {}

  render() {
    return (
      <Carousel>
		  <Carousel.Item>
		    <img className="d-block w-100" src={Banner} alt="First slide"/>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img className="d-block w-100" src={Banner} alt="Third slide"/>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img className="d-block w-100" src={Banner} alt="Third slide"/>
		  </Carousel.Item>
		</Carousel>
    );
  }
}
		